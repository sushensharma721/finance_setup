class Xpath:
    Xpath_For_DropDown_Selector        =             "//select[@id='dateRange']"
    Xpath_For_Selecting_1Month          =             "//select[@id='dateRange']/option[@value='1month']"
    Xpath_For_Symbol_Input              =             "//input[@id='symbol']"
    Xpath_For_GetData_Button            =             "//img[@id='get']"
    Xpath_For_DataLink_Download         =             "//a[contains(text(),'Download file in csv format')]"